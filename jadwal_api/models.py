from django.db import models
from django.contrib.auth.models import *
from datetime import timedelta

class Jadwal(models.Model):
    nama_matkul = models.CharField(max_length=100)
    userId = models.IntegerField(default=-1)
    tanggal = models.DateField(default=timezone.now())
    jamMulai = models.TimeField()
    jamSelesai = models.TimeField()
    totalDurasi = models.IntegerField(editable=False,default=0)
    jadwalId = models.AutoField(primary_key=True)
    
    class Meta:
        db_table = 'Jadwal'

'''
    def save(self, *args, **kwargs):
        if not self.contohId:
            self.totalDurasi = timezone.now()
        else:
            self.lastModifiedDate = timezone.now()

        self.tanggalHasilTambah = self.tanggal + timedelta(days=3)
        return super(Example, self).save(*args, **kwargs)
'''
