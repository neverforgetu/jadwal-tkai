# Generated by Django 2.0 on 2021-01-07 07:56

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal_api', '0002_auto_20210107_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='example',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2021, 1, 7, 7, 56, 27, 6572, tzinfo=utc)),
        ),
    ]
